document.addEventListener('DOMContentLoaded', function() {
    window.setTimeout( () => {
        var flashMessages = document.querySelectorAll('.flash-message');
        if (null != flashMessages) {
            for (var m of flashMessages) {
                m.classList.add('hidden');
                Object.assign(m.style, {display: 'none'})
            }
        }
    }, 5000);
});
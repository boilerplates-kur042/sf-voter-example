<?php

namespace App\Security\Voter;

use App\Entity\Post;
use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class PostVoter extends Voter
{
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function supports($attribute, $subject)
    {
        // check if attribute is supported and only vote on Post objects
        return in_array($attribute, [self::VIEW, self::EDIT]) && $subject instanceof Post;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        // user must be logged in i.e not null
        if (!$user instanceof User)
            return false;

        $post = $subject;

        switch($attribute) {
            case self::VIEW:
                return $this->canView($post, $user);

            case self::EDIT:
                return $this->canEdit($post, $user);
        }

        return false;
    }

    private function canView(Post $post, User $user)
    {
        // some logic to check if the user can view the post
        return true;
    }

    private function canEdit(Post $post, User $user)
    {
        return $user === $post->getOwner();
    }
}
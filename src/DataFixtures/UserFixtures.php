<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface $passwordEncoder
     */
    private $passwordEncoder;
    
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->passwordEncoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        // $faker = \Faker\Factory::create();
        // for ($i = 0; $i < 10; $i++)
        // {
        //     $user = new User;
        //     $user->setEmail($faker->freeEmail);
        //     $user->setPassword($this->passwordEncoder->encodePassword($user, 'voters'));
        //     $manager->persist($user);
        // }

        // $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends Fixture
{

    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create();
        $owner = $this->userRepository->findOneBy([
            'email' => 'amurazik@gmail.com',
        ]);
        for ($i = 0; $i < 60; $i++) {
            $post = (new Post)
                ->setTitle($faker->title)
                ->setContent($faker->text)
                ->setOwner($owner)
            ;
            $manager->persist($post);
        }

        $manager->flush();
    }
}

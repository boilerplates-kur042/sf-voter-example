<?php

namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var UrlGeneratorInterface $urlGenerator
     */
    private $urlGenerator;

    public function __construct(EntityManagerInterface $em, UrlGeneratorInterface $urlGenerator)
    {
        $this->em = $em;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/{postId}", name="post_single", methods={"GET"})
     * @ParamConverter("post", class="App\Entity\Post", options={"id"="postId"})
     */
    public function view(Post $post)
    {
        return $this->render('post/index.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/edit/{postId}", name="post_edit", methods={"GET|POST"})
     * @ParamConverter("post", class="App\Entity\Post", options={"id"="postId"})
     */
    public function edit(Request $request, Post $post)
    {
        $this->denyAccessUnlessGranted('edit', $post);
        
        return $this->render('post/post.html.twig', [
            'page_title' => 'Editing a post',
        ]);
    }

    /**
     * @Route("/new", name="post_add", methods={"GET|POST"})
     */
    public function new(Request $request)
    {
        $post = new Post;

        return $this->render('post/post.html.twig', [
            'page_title' => 'Adding new post',
        ]);
    }

    /**
     * @Route("/{postId}", name="post_delete", methods={"DELETE"}, requirements={"postId": "\d+"})
     * @ParamConverter("post", class="App\Entity\Post", options={"id"="postId"})
     */
    public function delete(Request $request, Post $post)
    {
        if ($post) {
            $this->em->remove($post);
            $this->em->flush();

            $this->addFlash('success', 'Post deleted successfully');
        } else {
            $this->addFlash('error', 'Error while deleting post');
        }
        return $this->redirectToRoute("app_home");
    }
}

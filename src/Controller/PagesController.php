<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Repository\PostRepository;
use App\Form\LoginFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class PagesController extends AbstractController
{
    /**
     * @var EntityManagerInterface $em
     */
    private $em;

    /**
     * @var UserRepository $userRepository
     */
    private $userRepository;

    public function __construct(EntityManagerInterface $manager, UserRepository $userRepo)
    {
        $this->em = $manager;
        $this->userRepository = $userRepo;
    }


    /**
     * @Route("/", name="app_home", methods={"GET|POST"})
     */
    public function home(Request $request, PostRepository $postRepository)
    {
        $user = new User();
        $loginForm = $this->createForm(LoginFormType::class, $user);
        $loginForm->handleRequest($request);

        if ($request->isMethod('POST')) {
            // $user->setEmail($request->request->get('email'));
            $res = $this->userRepository->findOneBy([
                'email' => $user->getEmail(),
            ]);
        }

        $posts = $postRepository->findAll();
        return $this->render('pages/home.html.twig', [
            'login_form' => $loginForm->createView(),
            'posts' => $posts,
        ]);
    }
}
